package uet.oop.bomberman.entities;


import uet.oop.bomberman.entities.bomb.Flame;
import uet.oop.bomberman.entities.character.Bomber;
import uet.oop.bomberman.entities.tile.Portal;
import uet.oop.bomberman.entities.tile.Brick;
import uet.oop.bomberman.entities.tile.item.BombPowerUpItem;
import uet.oop.bomberman.entities.tile.item.FlamePowerUpItem;
import uet.oop.bomberman.entities.tile.item.PowerUpItem;
import uet.oop.bomberman.entities.tile.item.SpeedPowerUpItem;
import uet.oop.bomberman.graphics.Screen;

import java.net.URL;
import java.util.LinkedList;

import static uet.oop.bomberman.Board.playSound;

// Enity de len nhau
public class LayeredEntity extends Entity {
	
	protected LinkedList<Entity> entitiesList = new LinkedList<>();
	private boolean canpass = false;
	
	public LayeredEntity(int x, int y, Entity ... entities) {
		xp = x;
		yp = y;
		
		for (int i = 0; i < entities.length; i++) {
			entitiesList.add(entities[i]);
			
			if(i > 1) {
				if(entities[i] instanceof Brick)
					((Brick)entities[i]).addBelowSprite(entities[i-1].getSprite());
			}
		}
	}
	
	@Override
	public void update() {
		clearRemoved();
		getTopEntity().update();
	}

	public int getLength(){
		return entitiesList.size();
	}
	
	@Override
	public void render(Screen screen) {
		getTopEntity().render(screen);
	}
	
	public Entity getTopEntity() {
		
		return entitiesList.getLast();
	}
	
	private void clearRemoved() {
		Entity top  = getTopEntity();
		
		if(top.isRemoved())  {
			entitiesList.removeLast();
		}
	}
	
	public void addBeforeTop(Entity e) {
		entitiesList.add(entitiesList.size() - 1, e);
	}
	
	@Override
	public boolean collide(Entity e) {
		if (this.getTopEntity() instanceof Brick && e instanceof Flame){
			Entity e1 = this.getTopEntity();
			((Brick)e1).destroy();
			canpass = true;

		}

		if (this.getTopEntity() instanceof PowerUpItem && e instanceof Bomber){
			Entity item = this.getTopEntity();
			URL resource = getClass().getResource("/music/smb_1-up.mp3");

			playSound(resource.getPath());
			item.remove();
			if (item instanceof FlamePowerUpItem) ((FlamePowerUpItem)item).use();
			if (item instanceof BombPowerUpItem) ((BombPowerUpItem)item).use();
			if (item instanceof SpeedPowerUpItem) ((SpeedPowerUpItem)item).use();

		}
		if (this.getTopEntity() instanceof Portal && e instanceof Bomber){
			((Portal)this.getTopEntity()).collide(e);
		}
		return canpass;
	}

}
