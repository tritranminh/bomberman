package uet.oop.bomberman.entities;

import uet.oop.bomberman.graphics.IRender;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.level.FixedPosition;

public abstract class Entity implements IRender {

	protected double xp, yp;
	protected boolean removed = false;
	protected Sprite sprite;

	@Override
	public abstract void update();

	@Override
	public abstract void render(Screen screen);
	
	public void remove() {
		removed = true;
	}
	
	public boolean isRemoved() {
		return removed;
	}
	
	public Sprite getSprite() {
		return sprite;
	}

	public abstract boolean collide(Entity e);
	
	public double getX() {
		return xp;
	}
	
	public double getY() {
		return yp;
	}
	
	public int getXTile() {
		return FixedPosition.pixelToTile(xp + sprite.SIZE / 2);
	}
	
	public int getYTile() {
		return FixedPosition.pixelToTile(yp - sprite.SIZE / 2);
	}
}
