package uet.oop.bomberman.entities.bomb;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.LayeredEntity;
import uet.oop.bomberman.entities.character.Bomber;
import uet.oop.bomberman.entities.character.Runner;
import uet.oop.bomberman.graphics.Screen;

public class Flame extends Entity {

	protected Board board;
	protected int direction;
	private int radius;
	protected int xOrigin, yOrigin;
	protected FlameSegment[] flameSegments = new FlameSegment[0];

	public Flame(int x, int y, int direction, int radius, Board board) {
		xOrigin = x;
		yOrigin = y;
		xp = x;
		yp = y;
		this.direction = direction;
		this.radius = radius;
		this.board = board;
		createFlameSegments();
	}

	// create segment, 1 segment = 1 dvdd
	private void createFlameSegments() {
		flameSegments = new FlameSegment[calculatePermitedDistance()];

		// last trang thai cuoi cung cua segment
		boolean last;
		int x = (int) xp;
		int y = (int) yp;

		for (int i = 0; i < flameSegments.length; i ++)
		{
			if (i == flameSegments.length - 1) last = true;
			else last = false;
			switch (direction) {
				case 0: y--; break;
				case 1: x++; break;
				case 2: y++; break;
				case 3: x--; break;
			}
			flameSegments[i] = new FlameSegment(x,y, direction,last);
		}

	}

	// Tinh do dai ten lua cham vao brick, wall thi dung lai
	private int calculatePermitedDistance() {
		int radius = 0;
		int x = (int) xp;
		int y = (int) yp;
		while(radius < this.radius) {
			if(direction == 0) y--;
			if(direction == 1) x++;
			if(direction == 2) y++;
			if(direction == 3) x--;

			Entity a = board.getEntity(x, y,null);

			if(a instanceof Bomber) ++radius;

			if(!a.collide(this))
				break;
			if (a.collide(this) && (a instanceof LayeredEntity) && ((LayeredEntity) a).getLength()>1) {
				radius ++;
				break;
			}
			++radius;
		}

		return radius;
	}
	
	public FlameSegment flameSegmentAt(int x, int y) {
		for (FlameSegment flameSegment : flameSegments) {
			if (flameSegment.getX() == x && flameSegment.getY() == y)
				return flameSegment;
		}
		return null;
	}

	@Override
	public void update() {}
	
	@Override
	public void render(Screen screen) {
		for (FlameSegment flameSegment : flameSegments) {
			flameSegment.render(screen);
		}
	}

	@Override
	public boolean collide(Entity e) {

		if (e instanceof Runner) {
			((Runner) e).kill();
		}
        return true;
	}
}
