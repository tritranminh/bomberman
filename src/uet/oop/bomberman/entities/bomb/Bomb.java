package uet.oop.bomberman.entities.bomb;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.entities.DynamicEntitiy;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.character.Bomber;
import uet.oop.bomberman.entities.character.Runner;
import uet.oop.bomberman.entities.character.mobs.Mob;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.level.FixedPosition;

import java.net.URL;

public class Bomb extends DynamicEntitiy {

	protected double timeToExplode = 60 * 2; //2 seconds
	public int timeAfter = 20;
	
	protected Board board;
	protected Flame[] flames;
	protected boolean exploded = false;
	protected boolean allowedToPass = true;
	
	public Bomb(int x, int y, Board board) {
		xp = x;
		yp = y;
		this.board = board;
		sprite = Sprite.bomb;
	}
	
	@Override
	public void update() {
		if(timeToExplode > 0)
			timeToExplode--;
		else {
			if(!exploded)
				explosion();
			else
				updateFlames();
			
			if(timeAfter > 0)
				timeAfter--;
			else
				remove();
		}
		animate();
	}
	
	@Override
	public void render(Screen screen) {
		if(exploded) {
			sprite =  Sprite.bomb_exploded2;
			renderFlames(screen);
		} else
			sprite = Sprite.movingSprite(Sprite.bomb, Sprite.bomb_1, Sprite.bomb_2, animate, 60);
		
		int xt = (int) xp << 4;
		int yt = (int) yp << 4;
		
		screen.renderEntity(xt, yt , this);
	}
	
	public void renderFlames(Screen screen) {
		for (Flame flame : flames) {
			flame.render(screen);
		}
	}
	
	public void updateFlames() {
		for (Flame flame : flames) {
			flame.update();
		}
	}

    //Xử lý Bomb nổ
	public void explode() {
		timeToExplode = 0;
	}
	protected void explosion() {
		allowedToPass = true;
		exploded = true;
		URL resource = getClass().getResource("/music/smb_blast.mp3");

		Board.playSound(resource.getPath());


        int x = (int) xp;
        int y = (int) yp;

		Entity entity = board.getEntityAt(x,y);
		if (entity != null) {
			if (entity instanceof Runner) ((Runner) entity).kill();
		}

		flames = new Flame[4];

        for (int i = 0; i < flames.length ; i ++)
            flames[i] = new Flame(x,y,i, Game.getBombRadius(), board);
	}
	
	public FlameSegment flameAt(int x, int y) {
		if(!exploded) return null;

		for (Flame flame : flames) {
			if (flame == null) return null;
			FlameSegment e = flame.flameSegmentAt(x, y);
			if (e != null) return e;
		}
		
		return null;
	}

	@Override
	public boolean collide(Entity e) {
		allowedToPass = true;
        if(e instanceof Bomber) {
            double x = e.getX() - FixedPosition.tileToPixel(getX());
            double y = e.getY() - FixedPosition.tileToPixel(getY());

            if(!(x >= -10 && x< 16 && y >= 1 && y <= 28)) {
                allowedToPass = false;
            }else return allowedToPass;
        }

        if(e instanceof Flame) {
        	explode();
            return true;
        }
		if(e instanceof Mob) {
			return false;
		}
        return true;
	}
}
