package uet.oop.bomberman.entities.bomb;

import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.character.Runner;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;


public class FlameSegment extends Entity {

	protected boolean last;

	public FlameSegment(int x, int y, int direction, boolean last) {
		xp = x;
		yp = y;
		this.last = last;

		switch (direction) {
			case 0:
				if(!last) {
					sprite = Sprite.explosion_vertical2;
				} else {
					sprite = Sprite.explosion_vertical_top_last2;
				}
			break;
			case 1:
				if(!last) {
					sprite = Sprite.explosion_horizontal2;
				} else {
					sprite = Sprite.explosion_horizontal_right_last2;
				}
				break;
			case 2:
				if(!last) {
					sprite = Sprite.explosion_vertical2;
				} else {
					sprite = Sprite.explosion_vertical_down_last2;
				}
				break;
			case 3: 
				if(!last) {
					sprite = Sprite.explosion_horizontal2;
				} else {
					sprite = Sprite.explosion_horizontal_left_last2;
				}
				break;
		}
	}
	
	@Override
	public void render(Screen screen) {
		int xt = (int) xp << 4;
		int yt = (int) yp << 4;
		
		screen.renderEntity(xt, yt , this);
	}
	
	@Override
	public void update() {}

	@Override
	public boolean collide(Entity e) {
		// Ten lua va cham
		if (e instanceof Runner) {
			((Runner) e).kill();
		}
		if (e instanceof Runner) {
			((Runner) e).kill();
		}

		return true;
	}
	

}