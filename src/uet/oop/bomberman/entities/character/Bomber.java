package uet.oop.bomberman.entities.character;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.bomb.Bomb;
import uet.oop.bomberman.entities.bomb.Flame;
import uet.oop.bomberman.entities.character.mobs.Mob;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.input.Keyboard;
import uet.oop.bomberman.level.FixedPosition;

import java.util.Iterator;
import java.util.List;

public class Bomber extends Runner {

    protected List<Bomb> bombList;
    protected Keyboard input;

    protected int timeBetweenPutBombs = 0;

    public Bomber(int x, int y, Board board) {
        super(x, y, board);
        bombList = this.board.getBombs();
        input = this.board.getInput();
        sprite = Sprite.player_right;
    }

    @Override
    public void update() {
        clearBombs();
        if (!alive) {
            afterKill();
            return;
        }

        if (timeBetweenPutBombs > 7500) timeBetweenPutBombs = 0;
        else timeBetweenPutBombs++;

        animate();
        calculateMove();
        detectPlaceBomb();
    }

    @Override
    public void render(Screen screen) {
        calculateXOffset();

        if (alive)
            chooseSprite();
        else
            sprite = Sprite.player_dead1;

        screen.renderEntity((int) xp, (int) yp - sprite.SIZE, this);
    }

    public void calculateXOffset() {
        int xScroll = Screen.calculateXOffset(board, this);
        Screen.setOffset(xScroll, 0);
    }

    private void detectPlaceBomb() {

        if(input.space && Game.getBombRate() > 0 && timeBetweenPutBombs >= 0) {

            int yt = FixedPosition.pixelToTile(yp -8);
            int xt = FixedPosition.pixelToTile(xp +8);

            this.placeBomb(xt,yt);

            Game.addBombRate(-1);

            timeBetweenPutBombs = -30;
        }
    }

    protected void placeBomb(int x, int y) {
        Bomb bomb = new Bomb(x,y, board);
        board.addBomb(bomb);
    }

    private void clearBombs() {
        Iterator<Bomb> bs = bombList.iterator();

        Bomb b;
        while (bs.hasNext()) {
            b = bs.next();
            if (b.isRemoved()) {
                bs.remove();
                Game.addBombRate(1);
            }
        }
    }

    @Override
    public void kill() {
        if (!alive) return;
        alive = false;
    }

    @Override
    protected void afterKill() {
        if (timeAfter > 0)
            --timeAfter;
        else {
            if (bombList.size() == 0) {
                    board.endGame();
            }
        }
    }

    @Override
    protected void calculateMove() {
        // Input up, right, down, left
        int xa = 0, ya = 0;
        if(input.up) ya--;
        if(input.down) ya++;
        if(input.left) xa--;
        if(input.right) xa++;

        if(xa != 0 || ya != 0)  {
            move(xa * Game.getBomberSpeed(), ya * Game.getBomberSpeed());
            moving = true;
        } else {
            moving = false;
        }
    }

    @Override
    public boolean canMove(double x, double y) {
        if (!alive) return false;

        for (int c = 0; c < 4; c++) {
            double xt = FixedPosition.pixelToTile((xp + x) + c % 2 * 11);
            double yt = FixedPosition.pixelToTile((yp + y) + c / 2 * 12 - 13) ;

            Entity a = board.getEntity(xt, yt, this);

            if(!a.collide(this))
                return false;
        }

        return true;
    }

    @Override
    public void move(double xa, double ya) {

        if(xa > 0) direction = 1;
        if(xa < 0) direction = 3;
        if(ya > 0) direction = 2;
        if(ya < 0) direction = 0;

        if(canMove(0, ya)) {
            yp += ya;
        }

        if(canMove(xa, 0)) {
            xp += xa;
        }

    }

    @Override
    public boolean collide(Entity e) {
        if(e instanceof Flame) {
            kill();
            return false;
        }

        if(e instanceof Mob) {
            kill();
            return false;
        }
        return true;
    }

    private void chooseSprite() {
        switch (direction) {
            case 0:
                sprite = Sprite.player_up;
                if (moving) {
                    sprite = Sprite.movingSprite(Sprite.player_up_1, Sprite.player_up_2, animate, 20);
                }
                break;
            case 1:
                sprite = Sprite.player_right;
                if (moving) {
                    sprite = Sprite.movingSprite(Sprite.player_right_1, Sprite.player_right_2, animate, 20);
                }
                break;
            case 2:
                sprite = Sprite.player_down;
                if (moving) {
                    sprite = Sprite.movingSprite(Sprite.player_down_1, Sprite.player_down_2, animate, 20);
                }
                break;
            case 3:
                sprite = Sprite.player_left;
                if (moving) {
                    sprite = Sprite.movingSprite(Sprite.player_left_1, Sprite.player_left_2, animate, 20);
                }
                break;
            default:
                sprite = Sprite.player_right;
                if (moving) {
                    sprite = Sprite.movingSprite(Sprite.player_right_1, Sprite.player_right_2, animate, 20);
                }
                break;
        }
    }
}
