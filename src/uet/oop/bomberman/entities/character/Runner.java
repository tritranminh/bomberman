package uet.oop.bomberman.entities.character;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.entities.DynamicEntitiy;
import uet.oop.bomberman.graphics.Screen;

public abstract class Runner extends DynamicEntitiy {
	
	protected Board board;
	protected int direction = -1;
	protected boolean alive = true;
	protected boolean moving = false;
	public int timeAfter = 40;
	
	public Runner(int x, int y, Board board) {
		xp = x;
		yp = y;
		this.board = board;
	}
	
	@Override
	public abstract void update();
	
	@Override
	public abstract void render(Screen screen);

	protected abstract void calculateMove();
	
	protected abstract void move(double xa, double ya);

	public abstract void kill();

	protected abstract void afterKill();

	protected abstract boolean canMove(double x, double y);

	protected double getXMessage() {
		return (xp * Game.SCALE) + (sprite.SIZE / 2 * Game.SCALE);
	}
	
	protected double getYMessage() {
		return (yp * Game.SCALE) - (sprite.SIZE / 2 * Game.SCALE);
	}
	
}
