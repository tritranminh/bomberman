package uet.oop.bomberman.entities.character.mobs.ai;

import java.util.Random;

public abstract class AI {

	protected Random random = new Random();

	//xuống/phải/trái/lên 0/1/2/3

	public abstract int nextDirection();
}
