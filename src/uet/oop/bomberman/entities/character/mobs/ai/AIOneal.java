package uet.oop.bomberman.entities.character.mobs.ai;

import uet.oop.bomberman.entities.character.Bomber;
import uet.oop.bomberman.entities.character.mobs.Mob;

public class AIOneal extends AI {
	Bomber bomber;
	Mob mob;
	
	public AIOneal(Bomber bomber, Mob e) {
		this.bomber = bomber;
		this.mob = e;
	}

	@Override
	public int nextDirection() {

		if(bomber == null)
			return random.nextInt(4);

		int nextStep = random.nextInt(2);

		if(nextStep == 1) {
			int v = horizontalPath();
			if(v != -1)
				return v;
			else
				return verticalPath();

		} else {
			int h = verticalPath();

			if(h != -1)
				return h;
			else
				return horizontalPath();
		}
	}

	protected int verticalPath() {
		if(bomber.getXTile() < mob.getXTile())
			return 3;
		else if(bomber.getXTile() > mob.getXTile())
			return 1;

		return -1;
	}

	protected int horizontalPath() {
		if(bomber.getYTile() < mob.getYTile())
			return 0;
		else if(bomber.getYTile() > mob.getYTile())
			return 2;
		return -1;
	}
}
