package uet.oop.bomberman.entities.character.mobs;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.entities.character.mobs.ai.AIOneal;
import uet.oop.bomberman.graphics.Sprite;

public class Minvo extends Mob {
    public Minvo(int x, int y, Board board) {
        super(x, y, board, Sprite.minvo_dead, Game.getBomberSpeed() * 2, 800);

        sprite = Sprite.minvo_right1;

        direction  = 0;
    }

    @Override
    protected void chooseSprite() {
        switch(direction) {
            case 0:
            case 1:
                if(moving)
                    sprite = Sprite.movingSprite(Sprite.minvo_right1, Sprite.minvo_right2, Sprite.minvo_right3, animate, 60);
                else
                    sprite = Sprite.minvo_left1;
                break;
            case 2:
            case 3:
                if(moving)
                    sprite = Sprite.movingSprite(Sprite.minvo_left1, Sprite.minvo_left2, Sprite.minvo_left3, animate, 60);
                else
                    sprite = Sprite.minvo_left1;
                break;
        }
    }
}
