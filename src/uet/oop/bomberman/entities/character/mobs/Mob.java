package uet.oop.bomberman.entities.character.mobs;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.bomb.Flame;
import uet.oop.bomberman.entities.character.Runner;
import uet.oop.bomberman.entities.character.mobs.ai.AI;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.level.FixedPosition;

public abstract class Mob extends Runner {

	protected int points;
	
	protected double speed;
	protected AI ai;

	protected final double MAX_STEPS;
	protected final double rest;
	protected double steps;
	
	protected int finalAnimation = 30;
	protected Sprite deadSprite;
	
	public Mob(int x, int y, Board board, Sprite dead, double speed, int points) {
		super(x, y, board);
		
		this.points = points;
		this.speed = speed;
		
		MAX_STEPS = Game.TILES_SIZE / this.speed;

		rest = (MAX_STEPS - (int) MAX_STEPS) / MAX_STEPS;
		steps = MAX_STEPS;
		
		timeAfter = 20;
		deadSprite = dead;
	}
	
	@Override
	public void update() {
		animate();
		
		if(!alive) {
			afterKill();
			return;
		}
		
		if(alive)
			calculateMove();
	}
	
	@Override
	public void render(Screen screen) {
		if(alive)
			chooseSprite();
		else {
			if(timeAfter > 0) {
				sprite = deadSprite;
				animate = 0;
			} else {
				sprite = Sprite.movingSprite(Sprite.mob_dead1, Sprite.mob_dead2, Sprite.mob_dead3, animate, 60);
			}
		}
		screen.renderEntity((int)xp, (int)yp - sprite.SIZE, this);
	}
	
	@Override
	public void calculateMove() {
		int ya = 0; int xa = 0;

		if(steps <= 0){
			direction = ai.nextDirection();
			steps = MAX_STEPS;
		}

		if(direction == 0) ya--;
		if(direction == 2) ya++;
		if(direction == 3) xa--;
		if(direction == 1) xa++;

		if(canMove(xa, ya)) {
			steps -= 1 ;
			move(xa * speed, ya * speed);
			moving = true;
		} else {
			steps = 0;
			moving = false;
		}
	}
	
	@Override
	public void move(double xa, double ya) {
		if(!alive) return;
		yp += ya;
		xp += xa;
	}
	
	@Override
	public boolean canMove(double x, double y) {
		if (!alive) return false;

		for (int c = 0; c < 4; c++) {
			double xt = FixedPosition.pixelToTile((xp + x) + c % 2 * 11);
			double yt = FixedPosition.pixelToTile((yp + y) + c / 2 * 12 - 13) ;
			Entity a = board.getEntity(xt, yt, this);
			if(!a.collide(this))
				return false;
		}
		return true;
	}

	@Override
	public boolean collide(Entity e) {
		// Va cham flame
		if(e instanceof Flame) {
			kill();
			return true;
		}
		return true;
	}
	
	@Override
	public void kill() {
		if(!alive) return;
		alive = false;

//		board.addPoints(points);
	}
	
	
	@Override
	protected void afterKill() {
		if(timeAfter > 0) --timeAfter;
		else {
			if(finalAnimation > 0) --finalAnimation;
			else
				remove();
		}
	}
	
	protected abstract void chooseSprite();
}
