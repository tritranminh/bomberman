package uet.oop.bomberman.entities.tile;


import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.bomb.Flame;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.level.FixedPosition;

public class Brick extends Tile {

	private final int MAX_ANIMATE = 7500;
	private int animate = 0;
	protected boolean destroyed = false;
	protected int timeToDisapear = 20;
	protected Sprite belowSprite = Sprite.grass;

	public Brick(int x, int y, Sprite sprite) {
		super(x, y, sprite);
	}

	@Override
	public void update() {
		if(destroyed) {
			if(animate < MAX_ANIMATE) animate++; else animate = 0;
			if(timeToDisapear > 0)
				timeToDisapear--;
			else
				remove();
		}
	}

	public void destroy() {
		destroyed = true;
	}

	@Override
	public boolean collide(Entity e) {
		if (e instanceof Flame) {
			destroy();
			return true;
		}
		return false;

	}

	public void addBelowSprite(Sprite sprite) {
		belowSprite = sprite;
	}

	protected Sprite movingSprite(Sprite normal, Sprite x1, Sprite x2) {
		int calc = animate % 30;

		if(calc < 10) {
			return normal;
		}

		if(calc < 20) {
			return x1;
		}

		return x2;
	}
	
	@Override
	public void render(Screen screen) {
		int x = FixedPosition.tileToPixel(xp);
		int y = FixedPosition.tileToPixel(yp);
		
		if(destroyed) {
			sprite = movingSprite(Sprite.brick_exploded, Sprite.brick_exploded1, Sprite.brick_exploded2);
			
			screen.renderEntityWithBelowSprite(x, y, this, belowSprite);
		}
		else
			screen.renderEntity( x, y, this);
	}
	
}
