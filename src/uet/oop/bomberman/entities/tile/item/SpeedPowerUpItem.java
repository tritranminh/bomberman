package uet.oop.bomberman.entities.tile.item;

import uet.oop.bomberman.Game;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.graphics.Sprite;

public class SpeedPowerUpItem extends PowerUpItem {

	public SpeedPowerUpItem(int x, int y, Sprite sprite) {
		super(x, y, sprite);
	}

	@Override
	public boolean collide(Entity e) {
		return false;
	}
	public void use(){
		Game.addBomberSpeed(1);
	}
}
