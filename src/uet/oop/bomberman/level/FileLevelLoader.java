package uet.oop.bomberman.level;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.entities.LayeredEntity;
import uet.oop.bomberman.entities.character.Bomber;
import uet.oop.bomberman.entities.character.mobs.*;
import uet.oop.bomberman.entities.tile.Grass;
import uet.oop.bomberman.entities.tile.Portal;
import uet.oop.bomberman.entities.tile.Wall;
import uet.oop.bomberman.entities.tile.Brick;
import uet.oop.bomberman.entities.tile.item.BombPowerUpItem;
import uet.oop.bomberman.entities.tile.item.FlamePowerUpItem;
import uet.oop.bomberman.entities.tile.item.SpeedPowerUpItem;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;

import java.io.*;

public class FileLevelLoader {
	protected int width = 24, height = 24;
	protected int level;
	protected Board board;

	private static char[][] map;
	
	public FileLevelLoader(Board board, int level) {
		this.board = board;
		loadLevel(level);
	}

	public void loadLevel(int level) {
		String path = "/levels/Level" + level +".txt";

		BufferedReader br = null;

		try {

			InputStream is = this.getClass().getResourceAsStream(path);
			br = new BufferedReader(new InputStreamReader(is));

			String[] option = br.readLine().split(" ");
			width = Integer.parseInt(option[2]);
			height = Integer.parseInt(option[1]);
			this.level = Integer.parseInt(option[0]);

			map = new char[width][height];
			for (int i = 0; i< height; i++){
				String line = br.readLine();
				for (int j = 0; j< width; j++){
					map[j][i] = line.charAt(j);
				}
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}

		}
	}

	public void createEntities() {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (map[i][j]== '#') {
					int pos = i +j * width;
					Sprite sprite = Sprite.wall;
					board.addEntity(pos, new Wall(i, j, sprite));
				}
				else if (map[i][j] == '*') {
					board.addEntity(i + j * width,
							new LayeredEntity(i, j,
									new Grass(i, j, Sprite.grass),
									new Brick(i, j, Sprite.brick)
							)
					);
				}
				else if(map[i][j]=='p'){
					board.addCharacter(new Bomber(FixedPosition.tileToPixel(i), FixedPosition.tileToPixel(j) + Game.TILES_SIZE, board));
					Screen.setOffset(0, 0);

					board.addEntity(i + j * width, new Grass(i, j, Sprite.grass));
				}
				else if (map[i][j]=='1'){
					board.addCharacter(new Balloon(FixedPosition.tileToPixel(i), FixedPosition.tileToPixel(j) + Game.TILES_SIZE, board));
					board.addEntity(i + j * width, new Grass(i, j, Sprite.grass));

				}
				else if (map[i][j] == '2'){
					board.addCharacter(new Oneal(FixedPosition.tileToPixel(i), FixedPosition.tileToPixel(j) + Game.TILES_SIZE, board));
					board.addEntity(i + j * width, new Grass(i, j, Sprite.grass));
				}
				else if (map[i][j] == '3'){
					board.addCharacter(new Doll(FixedPosition.tileToPixel(i), FixedPosition.tileToPixel(j) + Game.TILES_SIZE, board));
					board.addEntity(i + j * width, new Grass(i, j, Sprite.grass));
				}
				else if (map[i][j] == '4'){
					board.addCharacter(new Minvo(FixedPosition.tileToPixel(i), FixedPosition.tileToPixel(j) + Game.TILES_SIZE, board));
					board.addEntity(i + j * width, new Grass(i, j, Sprite.grass));
				}
				else if (map[i][j] == '5'){
					board.addCharacter(new Kondoria(FixedPosition.tileToPixel(i), FixedPosition.tileToPixel(j) + Game.TILES_SIZE, board));
					board.addEntity(i + j * width, new Grass(i, j, Sprite.grass));
				}
				else if (map[i][j] == 'f'){
					board.addEntity(i + j* width, new LayeredEntity(i, j,
							new Grass(i ,j, Sprite.grass),
							new FlamePowerUpItem(i ,j,Sprite.powerup_flames),
							new Brick(i ,j, Sprite.brick)) );
				}
				else if (map[i][j] == 's'){
					board.addEntity(i + j* width, new LayeredEntity(i, j,
							new Grass(i ,j, Sprite.grass),
							new SpeedPowerUpItem(i ,j,Sprite.powerup_speed),
							new Brick(i ,j, Sprite.brick)) );
				}
				else if (map[i][j] == 'b'){
					board.addEntity(i + j* width, new LayeredEntity(i, j,
							new Grass(i ,j, Sprite.grass),
							new BombPowerUpItem(i ,j,Sprite.powerup_bombs),
							new Brick(i ,j, Sprite.brick)) );
				}

				else if (map[i][j] == 'x'){
					board.addEntity(i + j* width, new LayeredEntity(i, j,
							new Grass(i ,j, Sprite.grass),
							new Portal(i ,j, board,Sprite.portal),
							new Brick(i ,j, Sprite.brick)) );
				}
				else {
					board.addEntity(i + j * width, new Grass(i, j, Sprite.grass));

				}
			}
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getLevel() {
		return level;
	}

}
