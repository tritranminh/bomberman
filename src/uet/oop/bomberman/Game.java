package uet.oop.bomberman;

import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.gameui.MainFrame;
import uet.oop.bomberman.input.Keyboard;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

/**
 * Tạo vòng lặp cho game, lưu trữ một vài tham số cấu hình toàn cục,
 * Gọi phương thức render(), update() cho tất cả các entity
 */
public class Game extends Canvas {

	public static final int TILES_SIZE = 16,
							WIDTH = TILES_SIZE * (31/2),
							HEIGHT = 13 * TILES_SIZE;

	public static int SCALE = 3;

	protected static int bombRate = 1;
	protected static double bombRadius = 1.0;
	protected static double bomberSpeed = 1.0;

	protected int screenDelay = 3;
	
	protected Keyboard input;
	protected boolean running = false;
	protected boolean paused = true;
	
	protected Board board;
	protected Screen screen;
	protected MainFrame frame;
	
	protected BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	protected int[] pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
	
	public Game(MainFrame frame) {
		this.frame = frame;
		this.frame.setTitle("BombermanGame");
		
		screen = new Screen(WIDTH, HEIGHT);
		input = new Keyboard();
		
		board = new Board(this, input, screen);
		addKeyListener(input);
	}
	
	
	private void renderGame() {
		BufferStrategy bs = getBufferStrategy();
		if(bs == null) {
			createBufferStrategy(3);
			return;
		}
		
		screen.clear();
		
		board.render(screen);
		
		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = screen.pixels[i];
		}
		
		Graphics g = bs.getDrawGraphics();
		
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		
		g.dispose();
		bs.show();
	}
	
	private void renderScreen() {
		BufferStrategy bs = getBufferStrategy();
		if(bs == null) {
			createBufferStrategy(3);
			return;
		}

		screen.clear();
		
		Graphics g = bs.getDrawGraphics();
		
		board.drawScreen(g);

		g.dispose();
		bs.show();
	}

	private void update() {
		input.update();
		board.update();
	}
	
	public void start() {
		running = true;
		
		long  lastTime = System.nanoTime();
		long timer = System.currentTimeMillis();
		final double ns = 1000000000.0 / 60.0; //nanosecond, 60 frames per second
		double delta = 0;

		requestFocus();
		while(running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while(delta >= 1) {
				update();
				delta--;
			}
			
			if(paused) {
				if(screenDelay <= 0) {
					board.setShow(-1);
					paused = false;
				}
				renderScreen();
			} else {
				renderGame();
			}

			if(System.currentTimeMillis() - timer > 1000) {
				frame.setTime(board.subtractTime());

				timer += 1000;
				frame.setTitle("BombermanGame");
				
				if(board.getShow() == 2)
					--screenDelay;
			}
		}
	}
	
	public static double getBomberSpeed() {
		return bomberSpeed;
	}
	
	public static int getBombRate() {
		return bombRate;
	}
	
	public static int getBombRadius() {
		return (int) Math.ceil(bombRadius / 4);
	}
	
	public static void addBomberSpeed(double i) {
		bomberSpeed += i;
	}
	
	public static void addBombRadius(double i) {
		bombRadius += i;
	}
	
	public static void addBombRate(int i) {
		bombRate += i;
	}

	public void resetScreenDelay() {
		screenDelay = 3;
	}

	public Board getBoard() {
		return board;
	}

	public boolean isPaused() {
		return paused;
	}
	
	public void pause() {
		paused = true;
	}
	
}
