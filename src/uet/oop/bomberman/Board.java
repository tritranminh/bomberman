package uet.oop.bomberman;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.bomb.Bomb;
import uet.oop.bomberman.entities.bomb.FlameSegment;
import uet.oop.bomberman.entities.character.Bomber;
import uet.oop.bomberman.entities.character.Runner;
import uet.oop.bomberman.graphics.IRender;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.input.Keyboard;
import uet.oop.bomberman.level.FileLevelLoader;


import java.awt.*;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Board implements IRender {
	protected FileLevelLoader fileLevelLoader;
	protected Game game;
	protected Keyboard input;
	protected Screen screen;
	
	public Entity[] entities;
	public List<Runner> runners = new ArrayList<>();
	protected List<Bomb> bombs = new ArrayList<>();
	
	protected int screenToShow = -1; //1:endgame, 2:changelevel, 3:paused
	
	protected int time = 180;
	
	public Board(Game game, Keyboard input, Screen screen) {
		this.game = game;
		this.input = input;
		this.screen = screen;

		loadLevel(1); //start in level 1
	}
	
	@Override
	public void update() {
		if( game.isPaused() ) return;
		
		updateEntities();
		updateCharacters();
		updateBombs();
//		updateMessages();
		detectEndGame();
		
		for (int i = 0; i < runners.size(); i++) {
			Runner a = runners.get(i);
			if(a.isRemoved()) runners.remove(i);
		}
	}

	@Override
	public void render(Screen screen) {
		if( game.isPaused() ) return;

		int x0 = Screen.xOffset >> 4;
		int x1 = (Screen.xOffset + screen.getWidth() + Game.TILES_SIZE) / Game.TILES_SIZE;
		int y0 = Screen.yOffset >> 4;
		int y1 = (Screen.yOffset + screen.getHeight()) / Game.TILES_SIZE;
		
		for (int y = y0; y < y1; y++) {
			for (int x = x0; x < x1; x++) {
				entities[x + y * fileLevelLoader.getWidth()].render(screen);
			}
		}
		renderBombs(screen);
		renderCharacter(screen);
	}

	public void nextLevel() {
		loadLevel(fileLevelLoader.getLevel() + 1);
	}

	public static void playSound(String path) {
		try {
			com.sun.javafx.application.PlatformImpl.startup(()->{});
			Media hit = new Media(new File(path).toURI().toString());
			MediaPlayer mediaPlayer = new MediaPlayer(hit);
			mediaPlayer.play();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void loadLevel(int level) {
		time = 180;
		screenToShow = 2;
		game.resetScreenDelay();
		game.pause();
		runners.clear();
		bombs.clear();
		
		try {
			fileLevelLoader = new FileLevelLoader(this, level);
			entities = new Entity[fileLevelLoader.getHeight() * fileLevelLoader.getWidth()];
			fileLevelLoader.createEntities();
			URL resource = getClass().getResource("/music/smb_start.mp3");
			playSound(resource.getPath());
		} catch (Exception e) {
			endGame();
		}
	}
	
	public void detectEndGame() {
		if(time <= 0)
			endGame();
	}
	
	public void endGame() {
		URL resource = getClass().getResource("/music/smb_gameover.mp3");
		playSound(resource.getPath());
		screenToShow = 1;
		game.resetScreenDelay();
		game.pause();
	}
	
	public boolean detectNoEnemies() {
		int total = 0;
		for (int i = 0; i < runners.size(); i++) {
			if(!(runners.get(i) instanceof Bomber))
				++total;
		}
		return total == 0;
	}
	
	public void drawScreen(Graphics g) {
		switch (screenToShow) {
			case 1:
				screen.drawEndGame(g);
				break;
			case 2:
				screen.drawChangeLevel(g, fileLevelLoader.getLevel());
				break;
			case 3:
				screen.drawPaused(g);
				break;
		}
	}
	
	public Entity getEntity(double x, double y, Runner m) {
		Entity res;
		
		res = getFlameSegmentAt((int)x, (int)y);
		if( res != null) return res;
		res = getBombAt(x, y);
		if( res != null) return res;
		res = getCharacterAtExcluding((int)x, (int)y, m);
		if( res != null) return res;
		res = getEntityAt((int)x, (int)y);
		
		return res;
	}
	
	public List<Bomb> getBombs() {
		return bombs;
	}
	
	public Bomb getBombAt(double x, double y) {
		Iterator<Bomb> bs = bombs.iterator();
		Bomb b;
		while(bs.hasNext()) {
			b = bs.next();
			if(b.getX() == (int)x && b.getY() == (int)y)
				return b;
		}
		return null;
	}

	public Bomber getBomber() {
		Iterator<Runner> itr = runners.iterator();
		
		Runner cur;
		while(itr.hasNext()) {
			cur = itr.next();
			
			if(cur instanceof Bomber)
				return (Bomber) cur;
		}

		return null;
	}

	public Runner getCharacterAtExcluding(int x, int y, Runner a) {
		Iterator<Runner> itr = runners.iterator();
		
		Runner cur;
		while(itr.hasNext()) {
			cur = itr.next();
			if(cur == a) {
				continue;
			}
			
			if(cur.getXTile() == x && cur.getYTile() == y) {
				return cur;
			}
		}
		return null;
	}
	
	public FlameSegment getFlameSegmentAt(int x, int y) {
		Iterator<Bomb> bs = bombs.iterator();
		Bomb b;
		while(bs.hasNext()) {
			b = bs.next();
			
			FlameSegment e = b.flameAt(x, y);
			if(e != null) {
				return e;
			}
		}
		return null;
	}
	
	public Entity getEntityAt(double x, double y) {
		return entities[(int)x + (int)y * fileLevelLoader.getWidth()];
	}
	
	public void addEntity(int pos, Entity e) {
		entities[pos] = e;
	}
	
	public void addCharacter(Runner e) {
		runners.add(e);
	}
	
	public void addBomb(Bomb e) {
		bombs.add(e);
	}

	protected void renderCharacter(Screen screen) {

		for (Runner runner : runners) runner.render(screen);
	}
	
	protected void renderBombs(Screen screen) {
		Iterator<Bomb> itr = bombs.iterator();
		
		while(itr.hasNext())
			itr.next().render(screen);
	}
	
	protected void updateEntities() {
		if( game.isPaused() ) return;
		for (int i = 0; i < entities.length; i++) {
			entities[i].update();
		}
	}
	
	protected void updateCharacters() {
		if( game.isPaused() ) return;
		Iterator<Runner> itr = runners.iterator();
		
		while(itr.hasNext() && !game.isPaused())
			itr.next().update();
	}
	
	protected void updateBombs() {
		if( game.isPaused() ) return;

		for (Bomb bomb : bombs) bomb.update();
	}

	public int subtractTime() {
		if(game.isPaused())
			return this.time;
		else
			return this.time--;
	}

	public Keyboard getInput() {
		return input;
	}

	public FileLevelLoader getLevel() {
		return fileLevelLoader;
	}

	public Game getGame() {
		return game;
	}

	public int getShow() {
		return screenToShow;
	}

	public void setShow(int i) {
		screenToShow = i;
	}

	public int getTime() {
		return time;
	}
	
	public int getWidth() {
		return fileLevelLoader.getWidth();
	}

	public int getHeight() {
		return fileLevelLoader.getHeight();
	}
	
}
