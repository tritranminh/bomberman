package uet.oop.bomberman.gameui;

import uet.oop.bomberman.Game;

import javax.swing.*;
import java.awt.*;

/**
 * Swing Panel hiển thị thông tin thời gian, điểm mà người chơi đạt được
 */
public class TimePanel extends JPanel {
	
	private JLabel timeLabel;

	public TimePanel(Game game) {
		setLayout(new GridLayout());
		
		timeLabel = new JLabel("Time: " + game.getBoard().getTime());
		timeLabel.setForeground(Color.white);
		timeLabel.setHorizontalAlignment(JLabel.CENTER);

		add(timeLabel);

		setBackground(Color.black);
		setPreferredSize(new Dimension(0, 40));
	}

	public void setTime(int t) {
		timeLabel.setText("Time: " + t);
	}
}
