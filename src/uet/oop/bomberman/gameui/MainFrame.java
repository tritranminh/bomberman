package uet.oop.bomberman.gameui;

import uet.oop.bomberman.Game;

import javax.swing.*;
import java.awt.*;

/**
 * Swing Frame chứa toàn bộ các component
 */
public class MainFrame extends JFrame {
	
	public GamePanel gamePanel;
	private JPanel containerPanel;
	private TimePanel timePanel;
	
	private Game game;

	public MainFrame() {
		
		containerPanel = new JPanel(new BorderLayout());
		gamePanel = new GamePanel(this);
		timePanel = new TimePanel(gamePanel.getGame());
		
		containerPanel.add(timePanel, BorderLayout.PAGE_START);
		containerPanel.add(gamePanel, BorderLayout.PAGE_END);
		
		game = gamePanel.getGame();
		
		add(containerPanel);
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);	
		
		game.start();
	}

	public void setTime(int time) {
		timePanel.setTime(time);
	}

}
